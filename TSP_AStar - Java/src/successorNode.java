import java.util.*;

public class successorNode{

	public int ID;
	public City node;
	public ArrayList<City> successors;

	public int parentID;
	public successorNode parentNode;

	public double gValue = 0;	
	public double hValue = 0;
	public double fValue = 0;


	public successorNode(int nodeId, City thisNodeRef, int parentNodeId, successorNode parentNodeRef){

		this.ID = nodeId;
		this.node = thisNodeRef;

		this.parentID = parentNodeId;	
		this.parentNode = parentNodeRef;
	}

	public void setSuccessors(ArrayList<City> successors){
		this.successors = successors;
	}

	public ArrayList<City> getSuccessors(){
		return successors;
	}
	
	public void setHValue(double temp_H_Value) {
		this.hValue = temp_H_Value;
	}
	
	public double getHValue(){
		return this.hValue;
	}

	public void setGValue(double newGValue){
		this.gValue = newGValue;
		
		this.fValue = this.hValue + this.getGValue();
	}

	public double getGValue(){
		return gValue;
	}
	
	public double getFValue(){
		return this.fValue;
	}

	public int getNodeID(){
		return ID;
	}

	public String getNodeName(){
		return node.getCityIndex();
	}

	public City getNodeInfo(){
		return this.node;
	}
	
	public int getNodeParentID(){
		return parentID;
	}

	public String getParentName(){
		return parentNode.getNodeName();
	}


	public void printPath(){
		successorNode currentNode = this; 
		successorNode parent = this.parentNode;
		if(currentNode.getNodeParentID() == -1){ //not null
			System.out.print(currentNode.getNodeName());
		} else{ 
			parent.printPath();
			System.out.print(currentNode.getNodeName());
		}

		/*		
		String parentNodeName = 
		String str = String.valueOf(path[0]);
		for(int i = 1; i < path.length; i++){
			str += (" ," + String.valueOf(path[i]));
		}

		return str;*/
	}

	

}
