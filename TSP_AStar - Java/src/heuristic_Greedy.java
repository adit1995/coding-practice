import java.util.ArrayList;
import java.util.Map;

//this heuristic uses a greedy approach to finding the shortest path 
public class heuristic_Greedy {

	public double calculate(successorNode start, Map<String, Float> map) {
		ArrayList<City> unvisited = new ArrayList<>(start.getSuccessors());

		City current = start.getNodeInfo();

		double Greedy_Cost = 0;

		while(unvisited.size() > 0){
			double leastDistance = Integer.MAX_VALUE;
			City tempCity = null;
			double temp = 0;

			for(City u: unvisited){
				String fromCurrentToU = current.getCityIndex()+u.getCityIndex();
				temp = map.get(fromCurrentToU);

				if (temp < leastDistance){
					leastDistance = temp;
					tempCity = u;
				}
			}

			unvisited.remove(tempCity);
			current = tempCity;
			Greedy_Cost+= leastDistance;

		}
		String str = current.getCityIndex() + "A";
		double edgeCost = map.get(str);
		return Greedy_Cost + edgeCost;
	}
}
