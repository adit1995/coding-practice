import java.util.ArrayList;
import java.util.Map;
//this heuristic uses a primitively coded version of the MST using Prim's algorithm + the (last node in MST list to start node) edge weight
public class heuristic_PRIM {

	public double calculate(successorNode start, Map<String, Float> map) {
		ArrayList<City> unvisited = new ArrayList<>(start.getSuccessors());
		ArrayList<City> visited = new ArrayList<>();

		visited.add(start.getNodeInfo());
		
		double Prims_Tree_Cost = 0;
		while(unvisited.size() > 0){
			double bestDist= Integer.MAX_VALUE;
			double temp = 0;

			City U = null;

			for(City v: visited){
				for(City u: unvisited){
					String fromVToU = v.getCityIndex()+u.getCityIndex();
					temp = map.get(fromVToU);
					if (temp < bestDist){
						bestDist = temp;
						U = u;
					}
				}
			}
			
			Prims_Tree_Cost+=bestDist;
			visited.add(U);
			unvisited.remove(U);
		}
		int index = visited.size() - 1;
		City lastCityinList = visited.get(index);
		String str = lastCityinList.getCityIndex() + "A";
		double edgeCost = map.get(str);
		return Prims_Tree_Cost + edgeCost;
	}

}
