import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Main {

	public static int noOfCities;

	public static void setNoOfCities(int cities){
		noOfCities = cities;
	}

	public static int getNoOfCities(){
		return noOfCities;
	}

	public static void main(String[] args) throws IOException {

		for(int folder = 1; folder <= 16; folder++){
			OUTER:
				for(int instance = 1; instance <=10; instance++){

					if(folder == 1){
						for(int i = 0; i < 10; i++)
							System.out.print("0.00\t1\t");
						break;
					}


					Path path = FileSystems.getDefault().getPath("randTSP\\" + folder, "instance_"+instance+".txt");

					Scanner inFile = new Scanner(path);
					setNoOfCities(inFile.nextInt());

					ArrayList<City> nodes = new ArrayList<>();
					City A = null;

					inFile.nextLine(); //skip whitespace

					boolean cityA = true;
					while(inFile.hasNextLine()){
						String line = inFile.nextLine();
						String[] cityFeatures = line.split(" ");	
						if (cityA){
							A = new City(cityFeatures[0], Integer.parseInt(cityFeatures[1]), Integer.parseInt(cityFeatures[2]));
							nodes.add(A);
							cityA = false;				
						}else{
							nodes.add(new City(cityFeatures[0], Integer.parseInt(cityFeatures[1]), Integer.parseInt(cityFeatures[2])));
						}	
					}

					inFile.close();

					//Generate city map
					Map<String, Float> mapInstance = cityMap.generateCityMap(nodes);


					aStar aStarObj = new aStar();

					aStarObj.aStar(A, nodes, mapInstance);

					if (folder == 2){
						double Xdiff = nodes.get(0).getX() - nodes.get(1).getX();
						double Ydiff = nodes.get(0).getY() - nodes.get(1).getY();
						double printout = Math.sqrt(Ydiff*Ydiff + Xdiff*Xdiff);

						System.out.print(String.format("%.2f", printout) + "\t2\t");
						continue OUTER;
					}
				}

		System.out.println();
		}
	}

}
