import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;

public class aStar {

	int nodeID = 0; //global successor node ID counter

	public void aStar(City start, ArrayList<City> nodes, Map<String, Float> map){ //start city, city list and map with edges

		Comparator<successorNode> comparator = new SuccessorComparator(); //Override comparator compare func for successor nodes for queues
		PriorityQueue<successorNode> CLOSED = new PriorityQueue<>(11, comparator); //a priority queue of a set of nodes that have been evaluated and expanded
		PriorityQueue<successorNode> OPEN = new PriorityQueue<>(11, comparator); //a priority queue of a set of nodes that are yet to be evaluated

		heuristic_PRIM HP = new heuristic_PRIM();
		heuristic_Greedy HG = new heuristic_Greedy();

		// A is first element, create A successorNode as parent node, generate A's successors manually and push into OPEN
		successorNode A = new successorNode(nodeID, start, -1, null);

		//create As successor list
		ArrayList<City> successorListA = new ArrayList<>();		
		successorListA.addAll(nodes); 
		successorListA.remove(start); 			
		A.setSuccessors(successorListA);

		//A's G-Value is 0
		A.setGValue(0);

		//creating nodes for the successors of A
		ArrayList<City> localSuccessorsA = A.getSuccessors();

		for(City successor: localSuccessorsA){

			//create A's successor
			successorNode newSuccessor = new successorNode(++nodeID, successor, A.getNodeID(), A);

			ArrayList<City> localSuccessorList = new ArrayList<>();
			localSuccessorList.addAll(localSuccessorsA);
			localSuccessorList.remove(successor);
			newSuccessor.setSuccessors(localSuccessorList);

			//Calculate heuristic value of the successor node in question
			double temp_H_Value_PRIM = HP.calculate(newSuccessor,map);
			double temp_H_Value_Greedy = HG.calculate(newSuccessor, map);
			double temp_H_Value = 0;

			newSuccessor.setHValue(temp_H_Value); //insert the choice of heuristic [.._PRIM/.._Greedy/..]

			// get g value of new successor [parent g value + edge weight]
			String mapIndex = A.getNodeName() + newSuccessor.getNodeName();
			double mapValue = map.get(mapIndex);
			double temp_G_Value = A.getGValue() + mapValue;
			newSuccessor.setGValue(temp_G_Value);


			//enqueue into OPEN queue 
			OPEN.add(newSuccessor);
		}
		CLOSED.add(A);

		while(!OPEN.isEmpty()){
			successorNode current = OPEN.poll();

			if(current.getNodeName().equals("A") && current.getNodeID() > 0){
				while(true){
					System.out.print(String.format("%.2f", current.getGValue()) + "\t" + nodeID + "\t");
					//System.out.print("\t" + String.format("%.2f", current.getGValue())+ "\t" + nodeID + "\t");
					break;
				}
				break;
			}else{

				ArrayList<City> localSuccessors = current.getSuccessors();

				for(City successor: localSuccessors){

					//create current's successors
					successorNode newSuccessor = new successorNode(++nodeID, successor, current.getNodeID(), current);

					ArrayList<City> localSuccessorList = new ArrayList<>();
					localSuccessorList.addAll(localSuccessors);
					localSuccessorList.remove(successor);

					if(localSuccessorList.isEmpty() && !current.getNodeName().equals("A")){
						localSuccessorList.add(start);
						newSuccessor.setSuccessors(localSuccessorList);

						//Calculate heuristic value of the successor node in question
						double temp_H_Value_PRIM = HP.calculate(newSuccessor, map);
						double temp_H_Value_Greedy = HG.calculate(newSuccessor, map);
						double temp_H_Value = 0;

						newSuccessor.setHValue(temp_H_Value); //insert the choice of heuristic [.._PRIM/.._Greedy/..]


						// get g value of new successor [parent g value + edge weight]
						String mapIndex = current.getNodeName() + newSuccessor.getNodeName();
						double mapValue = map.get(mapIndex);
						double temp_G_Value = current.getGValue() + mapValue;
						newSuccessor.setGValue(temp_G_Value);

						//enqueue into OPEN queue 
						OPEN.add(newSuccessor);
					} else {
						newSuccessor.setSuccessors(localSuccessorList);

						//Calculate heuristic value of the successor node in question
						double temp_H_Value_PRIM = HP.calculate(newSuccessor,map);
						double temp_H_Value_Greedy = HG.calculate(newSuccessor, map);
						double temp_H_Value = 0;

						newSuccessor.setHValue(temp_H_Value); //insert the choice of heuristic [.._PRIM/.._Greedy/..]

						// get g value of new successor [parent g value + edge weight]
						String mapIndex = current.getNodeName() + newSuccessor.getNodeName();
						double mapValue = map.get(mapIndex);
						double temp_G_Value = current.getGValue() + mapValue;
						newSuccessor.setGValue(temp_G_Value);

						//enqueue into OPEN queue 
						OPEN.add(newSuccessor);
					}

				}
			}
			CLOSED.add(current);
		}
	}
}
