import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class cityMap {
	
	static Map<String, Float > CityMap = new LinkedHashMap<>();
	
	//create a weighted non-directed map of the cities
	public static Map<String, Float> generateCityMap(ArrayList<City> nodes) {
		for (City c: nodes){
			for(City c_dash: nodes){
					cityMap.addEdge(c,c_dash);
			}
		}
		
		return getCityMap();
		
	}
	
	//sub-function of generateCityMap
	public static void addEdge(City from, City to){
		String sourceToDestination = from.getCityIndex() + to.getCityIndex();
		
		float deltaY = from.getY()-to.getY();
		float deltaX = from.getX()-to.getX();
		float eucDist = (float) Math.sqrt(deltaY*deltaY + deltaX*deltaX);
		
		CityMap.put(sourceToDestination, eucDist);
		
	}
	
	//returns the cityMap
	public static Map<String, Float> getCityMap(){
		return CityMap;
	}

	
	
}
