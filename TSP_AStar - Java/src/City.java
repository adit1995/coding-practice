
public class City {

	public String cityIndex;
	public int X;
	public int Y;
	
	public City(String index, int xValue, int yValue){
		this.cityIndex = index;
		this.X = xValue;
		this.Y = yValue;
	}

	public String getCityIndex() {
		return cityIndex;
	}

	public void setCityIndex(String cityIndex) {
		this.cityIndex = cityIndex;
	}

	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getY() {
		return Y;
	}

	public void setY(int y) {
		Y = y;
	}
	
}
