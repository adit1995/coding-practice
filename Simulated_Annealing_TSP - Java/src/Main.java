import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	@SuppressWarnings("unused")
	private static int noOfCities;

	public static void setNoOfCities(int cities) {
		noOfCities = cities;
	}

	public static void main(String[] args) throws IOException {

		Path inFilePath = FileSystems.getDefault().getPath("randTSP\\", "problem36");

		Scanner inFile = new Scanner(inFilePath);
		setNoOfCities(inFile.nextInt());

		ArrayList<City> Cities = new ArrayList<>();

		inFile.nextLine(); // skip whitespace

		while (inFile.hasNextLine()) {
			String line = inFile.nextLine();
			String[] cityFeatures = line.split(" ");
			new City(cityFeatures[0], Integer.parseInt(cityFeatures[1]), Integer.parseInt(cityFeatures[2]));
			Cities.add(new City(cityFeatures[0], Integer.parseInt(cityFeatures[1]), Integer.parseInt(cityFeatures[2])));
		}

		inFile.close();

		//Annealing Schedules
		
		//Temperature_CoolingRate = 1000000_0.000001
		simulatedAnnealingTSP fam = null;
		for(int i = 1; i <= 5; i++){
			String fileName = "output_" + i + "_problem36_1000000_0.000001.csv";
			fam = new simulatedAnnealingTSP(Cities,1000000,0.000001, fileName);
			fam.doSomeAnnealing();
		}

		//Temperature_CoolingRate = 1000000_0.00005
		for(int i = 1; i <= 5; i++){
			String fileName2 = "output_" + i + "_problem36_1000000_0.00005.csv";
			fam = new simulatedAnnealingTSP(Cities,1000000,0.00005, fileName2);
			fam.doSomeAnnealing();
		}

		//Temperature_CoolingRate = 3000000_0.00005
		for(int i = 1; i <= 5; i++){
			String fileName3 = "output_" + i + "_problem36_3000000_0.00005.csv";
			fam = new simulatedAnnealingTSP(Cities,3000000,0.00005, fileName3);
			fam.doSomeAnnealing();
		}
	}
}
