import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class simulatedAnnealingTSP {

	int noOfCities; 

	ArrayList<City> currentTour = new ArrayList<>();
	double currentDistance = 0.0;

	ArrayList<City> bestTour = new ArrayList<>();	
	double bestDistance = 0.0;

	double temperature;
	double alpha;

	File file = null;
	FileWriter fw = null;

	//The constructor Generates the suboptimal tour and makes it the Current Tour [and the Best Tour] that doSomeAnnealing() works on
	public simulatedAnnealingTSP(ArrayList<City> cities, double temperature, double alpha, String fileName) throws IOException {

		this.noOfCities = cities.size() - 1;
		this.temperature = temperature;
		this.alpha = alpha;

		this.file = new File(fileName);
		file.createNewFile();

		//create first tour //simply copy the cities list into the tour list i.e the tour is A->B->C->D...->A
		for(City c: cities){
			currentTour.add(c);
		}
		currentTour.add(currentTour.get(0));

		//getDistance of first tour
		currentDistance = getTourDistance(currentTour);


		//bestTour = currentTour
		for(City c: currentTour){
			bestTour.add(c);
		}
		//bestDistance = currentDistance
		bestDistance = currentDistance;

		fw = new FileWriter(file);
		fw.write(printTourCostAndTemperature(bestTour, temperature, 0));
	}

	//Euclidean Distance calculator
	private double getTourDistance(ArrayList<City> tour) {
		double distance = 0.0;
		int tourLength = tour.size();

		for(int index = 1; index < tourLength; index++){
			double deltaX = tour.get(index).getX() - tour.get(index-1).getX();
			double deltaY = tour.get(index).getY() - tour.get(index-1).getY();
			double eucDist = Math.sqrt(deltaX*deltaX + deltaY*deltaY);

			distance += eucDist;
		}		
		return distance;
	}

	//The main Simulated Annealing function
	//Finds the best Tour with the least Tour Distance given that the temperature is greater than 1
	public void doSomeAnnealing() throws IOException{
		int i = 0;

		while(temperature > 1){

			ArrayList<City> tempTour = new ArrayList<>();
			double tempDistance = 0.0;
			double probability = 0.0;

			//Preserves the Current Tour
			for(City c: currentTour){
				tempTour.add(c);
			}
			int indexA;
			int indexB;

			//Random move swap within tempTour
			indexA = (int) ((Math.random() * noOfCities) + 1);
			indexB = (int) ((Math.random() * noOfCities) + 1);

			City tempA = tempTour.get(indexA);
			City tempB = tempTour.get(indexB);

			tempTour.set(indexA, tempB);
			tempTour.set(indexB, tempA);

			//Find the tempTour distance
			tempDistance = getTourDistance(tempTour);

			//Assess whether tempTour should be accepted as the new solution as a function of temperature
			if(currentDistance - tempDistance >= 0){
				currentTour = tempTour;
				currentDistance = tempDistance;
			}else{
				probability = Math.exp((currentDistance - tempDistance) / temperature);
				if(probability > Math.random()){
					currentTour = tempTour;
					currentDistance = tempDistance;
				}
			}

			//Cool the system
			temperature = temperature*(1-alpha);
			i++;

			//Keep the best Tour in memory
			if(currentDistance < bestDistance){
				bestTour = currentTour;
				bestDistance = currentDistance;
				fw.write(printTourCostAndTemperature(bestTour, temperature, i)); //write the new best tour to File for further analysis
			}
		}
		fw.close();
		System.out.println(this.file.getName() + ": "+ getTourDistance(bestTour));
	}

	private String printTourCostAndTemperature(ArrayList<City> bTour, double temp, int i){
		String str="";

		double tourCost = getTourDistance(bTour);

		str+=String.format("%.2f",tourCost);

		str += String.format(",%.2f\n", temp);

		return str;

	}
}
