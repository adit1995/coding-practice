'''
author: Aditya Kedia
'''
import pandas as pd
import math

'''
Calculate entropy of subgroups
'''
def calcEntropy(group):
    class1 = (group.loc[group.label == label[0], 'label'].count())/len(group)
    class2 = 1 - class1

    if class1 == 0.0:
        E = 0.0
    elif class2 == 0.0:
        E = 0.0
    else:
        E = -(class1 * math.log(class1, 2)) - (class2 * math.log(class2, 2))

    return E


'''
The lower the entropy of the system, the higher the Information Gain.
The logic here is to find the lowest entropy a feature selection makes
and then store that feature and the threshold along with the split groups.
Recursively continue splitting the groups to find the best attributes 
and threshold and store these selections in the tree variable
'''
def generateTree(entropy, data):
    #If entropy of a subgroup is 0, it is a leaf node
    if entropy == 0.0:
        nodeLabel = data.loc[0,'label']
        return nodeLabel

    b_entropy = entropy
    b_threshold = 0.0
    b_decisionAttribute = None
    decision = []

    for attribute in attributes:
        if attribute == 'label':
            # Go down the left branch
            leftBranch = generateTree(decision[3][1], decision[3][0])

            #Go down the right branch
            rightBranch = generateTree(decision[4][1], decision[4][0])

            # return [attribute, threshold, leftbranchfeatures, rightbranchfeatures]
            return [decision[2], decision[1], leftBranch, rightBranch]

        local = data.sort_values(by=attribute)
        left = pd.DataFrame(columns=attributes)

        for index, row in local.iterrows():
            #Split data into left and right groups
            left_data = data.iloc[[index]]
            left = left.append(left_data)
            list_of_left_indices = list(left.index.values)
            right_data = data.drop(list_of_left_indices)

            #calculate entropy of system with current left and right groups
            denom = (len(left.index) + len(right_data.index))
            left_entropy = calcEntropy(left)
            right_entropy = calcEntropy(right_data)
            temp_entropy = (len(left.index)/denom)*left_entropy + ((len(right_data.index)/denom)*right_entropy)

            if temp_entropy <= b_entropy:
                b_entropy = temp_entropy
                b_decisionAttribute = attribute
                b_threshold = (left.loc[left[attribute].idxmax(), attribute] + right_data.loc[right_data[attribute].idxmin(), attribute])/2
                left_group = left.reset_index(drop=True)
                right_group = right_data.reset_index(drop=True)
                decision = [b_entropy, b_threshold, b_decisionAttribute, [left_group,left_entropy], [right_group,right_entropy]]

# Prints the generated tree - for debug and image drawing purposes
def printTree(dt, tabs, branch):
    if dt == label[0] or dt == label[1]:
        print("|",('-' * (3*tabs)), branch, "Node:", dt)
        return
    else:
        if branch == 'root':
            print("|", ('-' * (3 * tabs)), branch, ":", dt[0], "<=", dt[1])
        else:
            print("|",('-'* (3*tabs)), branch, "Branch:", dt[0], "<=", dt[1])

    printTree(dt[2], tabs+1, 'Left')
    printTree(dt[3], tabs+1, 'Right')

# predict helper function
def predict(testSet, tree):
    colic, healthy = 0, 0
    for index, row in testSet.iterrows():
        if row['label'] == 'colic':
            colic = colic + 1
        else:
            healthy = healthy + 1

    print("\nroot: colic[", colic, "] healthy [",healthy,"]")

    predict_main(testSet, tree, 0)

# main predicting function
def predict_main(testSet, tree, tabs):
    currentAttribute, currentThreshold = tree[0], tree[1]

    local = testSet.sort_values(by=currentAttribute)
    left = pd.DataFrame(columns=attributes)
    right = pd.DataFrame(columns=attributes)

    #divide the testSet according to attribute and threshold
    for index, row in local.iterrows():
        if row[currentAttribute] <= currentThreshold:
            left_data = testSet.iloc[[index]]
            left = left.append(left_data)
        else:
            right_data = testSet.iloc[[index]]
            right = right.append(right_data)

    left = left.reset_index(drop=True)
    right = right.reset_index(drop=True)

    if list(left.index.values).__sizeof__() != 0:
        left_colic, left_healthy = 0, 0
        for index, row in left.iterrows():
            if row['label'] == 'colic':
                left_colic = left_colic + 1
            else:
                left_healthy = left_healthy + 1

        print(("\t"*tabs), currentAttribute, "<=", currentThreshold, ": colic[",left_colic,"] healthy[",left_healthy,"]")
        if calcEntropy(left) != 0.0:
            predict_main(left, tree[2], tabs+1)

    if list(right.index.values).__sizeof__() != 0:
        right_colic, right_healthy = 0, 0
        for index, row in right.iterrows():
            if row['label'] == 'colic':
                right_colic = right_colic + 1
            else:
                right_healthy = right_healthy + 1

        print(("\t"*tabs), currentAttribute, ">", currentThreshold, ": colic[", right_colic, "] healthy[", right_healthy, "]")
        if calcEntropy(right) != 0.0:
            predict_main(right, tree[3], tabs+1)



'''
Data import and cleanup
'''
attributes = ['K','Na','CL','HCO3','Endotoxin','Aniongap','PLA2','SDH','GLDH','TPP','Breath rate','PCV','Pulse Rate','Fibrinogen','Dimer','FibPerDim','label']
label = ['colic', 'healthy']

trainSet = pd.read_csv('..\horseTrain.txt', sep=",", header = None)
trainSet.columns = attributes
for i in range(len(trainSet['label'])):
    value = trainSet.at[i, 'label']
    value = value[:-1]
    trainSet.at[i, 'label'] = value

testSet = pd.read_csv('..\horseTest.txt', sep=",", header = None)
testSet.columns = attributes
for i in range(len(testSet['label'])):
    value = testSet.at[i, 'label']
    value = value[:-1]
    testSet.at[i, 'label'] = value


print("\ngenerating tree...")
tree = generateTree(calcEntropy(trainSet), trainSet)
printTree(tree,0,'root')

print("\npredicting...")
predict(testSet, tree)
